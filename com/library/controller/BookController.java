package library.controller;

import com.google.zxing.WriterException;
import library.document.Book;
import library.repository.BookRepo;
import library.service.QRService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
public class BookController {

    private final BookRepo bookRepo;
    private final QRService QRService;

    private final String ngrok = "http://25161271.ngrok.io";

    @GetMapping("/test")
    public void test() throws IOException, WriterException {
//        return "200";
    }

    @GetMapping("/qrAll")
    public String generateQrForGetAllBooks() throws IOException, WriterException {
//        QRService.getQR(ngrok + "/book/all", "all");
        return "file:///C://Users/RTatusko/Documents/marker/all.png";
    }


    //todo change save to post
    @GetMapping("/book/name/{bookName}")
    public Book saveSimpleBook(@PathVariable String bookName) throws IOException, WriterException {
        Book book = new Book();
        book.name = bookName;
        Book saved = bookRepo.save(book);
        log.info("book id -> " + saved.id);
        QRService.getQR(ngrok + "book" + saved.id, saved.id);

        return saved;
    }


    @GetMapping(value = "/book/{bookId}")
    public Book getBookById(@PathVariable String bookId) {
        return bookRepo.findById(bookId).get();
    }

    @GetMapping(value = "/book/all")
    public List<Book> getAllBooks() {
        return bookRepo.findAll();
    }
}
