package library.document;

import lombok.RequiredArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@RequiredArgsConstructor
@Document(collection = "books")
public class Book {
    @Id
    public String id;

    public String name;
    public String author;

    public Reader reader;
    public boolean isFree = true;
}
